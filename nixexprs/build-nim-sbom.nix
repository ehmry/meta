{
  lib,
  stdenv,
  fetchgit,
  fetchzip,
  runCommand,
  xorg,
  nim,
  nimOverrides,
}:

let
  fetchers = {
    fetchzip =
      { url, sha256, ... }:
      fetchzip {
        name = "source";
        inherit url sha256;
      };
    git =
      {
        fetchSubmodules ? false,
        leaveDotGit ? false,
        rev,
        sha256,
        url,
        ...
      }:
      fetchgit {
        inherit
          fetchSubmodules
          leaveDotGit
          rev
          sha256
          url
          ;
      };
  };

  filterPropertiesToAttrs =
    prefix: properties:
    lib.pipe properties [
      (builtins.filter ({ name, ... }: (lib.strings.hasPrefix prefix name)))
      (map (
        { name, value }:
        {
          name = lib.strings.removePrefix prefix name;
          inherit value;
        }
      ))
      builtins.listToAttrs
    ];

  buildNimCfg =
    { backend, components, ... }:
    let
      componentSrcDirs = map (
        { properties, ... }:
        let
          fodProps = filterPropertiesToAttrs "nix:fod:" properties;
          fod = fetchers.${fodProps.method} fodProps;
          srcDir = fodProps.srcDir or "";
        in
        if srcDir == "" then fod else "${fod}/${srcDir}"
      ) components;
    in
    runCommand "nim.cfg"
      {
        outputs = [
          "out"
          "src"
        ];
        nativeBuildInputs = [ xorg.lndir ];
      }
      ''
        pkgDir=$src/pkg
        cat << EOF >> $out
        backend:${backend}
        path:"$src"
        path:"$pkgDir"
        EOF
        mkdir -p "$pkgDir"
        ${lib.strings.concatMapStrings (d: ''
          lndir "${d}" "$pkgDir"
        '') componentSrcDirs}
      '';

  buildCommands = lib.attrsets.mapAttrsToList (
    output: input: ''
      nim compile $nimFlags --out:${output} ${input}
    ''
  );

  installCommands = lib.attrsets.mapAttrsToList (
    output: input: ''
      install -Dt $out/bin ${output}
    ''
  );

  asFunc = x: if builtins.isFunction x then x else (_: x);
in

sbomPath: args:

let
  f0 =
    let
      sbom =
        if builtins.isAttrs sbomPath then sbomPath else builtins.fromJSON (builtins.readFile sbomPath);
      properties = builtins.listToAttrs sbom.metadata.component.properties;
    in
    final: {
      strictDeps = true;
      pname = sbom.metadata.component.name;
      inherit (sbom.metadata.component) version;
      nimBackend = properties."nim:backend";

      configurePhase = ''
        runHook preConfigure
        echo "nim.cfg << ${final.nimCfg}"
        cat ${final.nimCfg} >> nim.cfg
        echo nimcache:\"$NIX_BUILD_TOP/nimcache\" >> nim.cfg
        runHook postConfigure
      '';

      buildPhase = ''
        runHook preBuild
        ${lib.strings.concatLines (buildCommands final.passthru.nimBin)}
        runHook postBuild
      '';

      installPhase = ''
        runHook preInstall
        ${lib.strings.concatLines (installCommands final.passthru.nimBin)}
        runHook postInstall
      '';

      passthru = {
        inherit sbom properties;
      };
    };
  f1 = asFunc (asFunc args);
  f2 = (
    final: prev:
    let
      sbom = prev.passthru.sbom;
    in
    {
      passthru = lib.attrsets.recursiveUpdate prev.passthru {
        nimBin = lib.pipe final.passthru.properties [
          (lib.attrsets.filterAttrs (name: value: lib.strings.hasPrefix "nim:bin:" name))
          (lib.attrsets.mapAttrs' (
            name: value: {
              name = lib.strings.removePrefix "nim:bin:" name;
              value = "${
                final.passthru.properties."nim:binDir" or (final.passthru.properties."nim:srcDir" or ".")
              }/${value}";
            }
          ))
        ];
      };

      nativeBuildInputs = (prev.nativeBuildInputs or [ ]) ++ [ nim ];

      nimCfg = buildNimCfg {
        backend = final.nimBackend;
        inherit (sbom) components;
      };
    }
  );

  f3 =
    final: prev:
    builtins.foldl' (
      prev:
      { name, ... }@component:
      if (builtins.hasAttr name nimOverrides) then
        (prev // (nimOverrides.${name} component final prev))
      else
        prev
    ) prev prev.passthru.sbom.components;

  composition =
    final:
    builtins.foldl' (p: f: p // (f final p)) (f0 final) [
      f1
      f2
      f3
    ];
in
stdenv.mkDerivation composition
