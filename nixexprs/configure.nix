{ pkgs ? import <nixpkgs> { }, lockPath, excludes ? [ ] }:

let
  inherit (pkgs) lib;
  buildPkgDir = depends:
    let
      fetchers = {
        fetchzip = { url, sha256, ... }:
          pkgs.fetchzip {
            name = "source";
            inherit url sha256;
          };
        git = { fetchSubmodules, leaveDotGit, rev, sha256, url, ... }:
          pkgs.fetchgit {
            inherit fetchSubmodules leaveDotGit rev sha256 url;
          };
      };
      srcDirs = map (attrs@{ method, srcDir, ... }:
        let fod = fetchers.${method} attrs;
        in if srcDir == "" then fod else "${fod}/${srcDir}") depends;
    in pkgs.runCommand "nim.cfg" {
      outputs = [ "out" "src" ];
      nativeBuildInputs = [ pkgs.xorg.lndir ];
    } ''
      pkgDir=$src/pkg
      cat << EOF >> $out
      noNimblePath
      path:"$src"
      path:"$pkgDir"
      EOF
      mkdir -p "$pkgDir"
      for d in ${toString srcDirs}
      do
        lndir "$d" "$pkgDir"
      done
    '';
in with builtins;
lib.pipe lockPath [
  readFile
  fromJSON
  (getAttr "depends")
  (filter ({ packages, ... }: !(any (pkg: elem pkg excludes) packages)))
  buildPkgDir
]
