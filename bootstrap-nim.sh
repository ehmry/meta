#! /usr/bin/env nix-shell
#! nix-shell -i bash --packages nim-unwrapped-2

pushd $1

case "$2" in
"koch")
	nim c koch
	;;
"nim")
	./koch boot -d:release
	rm -rf compiler/nim compiler/nim1 compiler/nim2 dist nimcache
	;;
esac
